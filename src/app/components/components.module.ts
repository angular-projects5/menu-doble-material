import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BreadcumbComponent } from './breadcumb/breadcumb.component';
import { ExampleComponent } from './example/example.component';
import { FormsModule } from '@angular/forms';
import { InicioComponent } from './inicio/inicio.component';
import { PanelComponent } from './panel/panel.component';
import { InfoGeneralComponent } from './info-general/info-general.component';
import { RegistroActividadComponent } from './registro-actividad/registro-actividad.component';
import { EntradasComponent } from './entradas/entradas.component';
import { ParonesComponent } from './parones/parones.component';
import { SalidasComponent } from './salidas/salidas.component';
import { EventosComponent } from './eventos/eventos.component';
import { CampingComponent } from './camping/camping.component';
import { EscaladaComponent } from './escalada/escalada.component';



@NgModule({
  declarations: [BreadcumbComponent, ExampleComponent, InicioComponent, PanelComponent, InfoGeneralComponent, RegistroActividadComponent, EntradasComponent, ParonesComponent, SalidasComponent, EventosComponent, CampingComponent, EscaladaComponent],
  imports: [
    CommonModule,
    FormsModule
  ],
  exports: [BreadcumbComponent]
})
export class ComponentsModule { }
