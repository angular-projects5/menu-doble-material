import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { GlobalService } from 'src/app/shared/services/global.service';
import { MenuItem } from 'src/app/shared/interfaces/menu-item';
import { MenuOptionsService } from 'src/app/shared/services/menu-options.service';
import { Logos } from 'src/app/shared/interfaces/toolbar';

import { first } from 'rxjs/operators';

@Component({
  selector: 'app-example',
  templateUrl: './example.component.html',
  styleUrls: ['./example.component.scss']
})
export class ExampleComponent implements OnInit {

  logo: string;
  logoToolbar: string;

  menu: MenuItem[];

  menuLong = false;
  showSemantic = false;

  fillerContent = Array.from({ length: 2 }, () =>
    `Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut
       labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco
       laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in
       voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat
       cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.`);

  constructor(
    private globalService: GlobalService,
    private menuOptionsService: MenuOptionsService
  ) {
  }

  ngOnInit(): void {

    this.menu = this.menuOptionsService.menu.getValue();
    this.menuLong = this.menu.length > 10;
    this.showSemantic = this.menu.some(item => item.isSemantic);

    const logo = this.menuOptionsService.logo.getValue();
    if (logo) {
      this.logo = logo.logo;
      this.logoToolbar = logo.toolbarLogo;
    }
  }

  // métodos de gestión del menú
  changeLogo() {
    if (!this.logo) {
      this.logo = 'assets/images/cascada.png';
      this.logoToolbar = 'assets/images/logo_blanco.png';
    } else {
      this.logo = undefined;
      this.logoToolbar = undefined;
    }
    const logoClass: Logos = {
      logo: this.logo,
      toolbarLogo: this.logoToolbar
    };
    this.menuOptionsService.setLogo(logoClass);
  }

  changeMenuLong() {
    this.menuLong = !this.menuLong;
    if (this.menuLong) {
      this.menu = this.globalService.getNavMenuLong();
    } else {
      this.menu = this.globalService.getNavMenu();
    }

    this.setSemanticItem();
  }

  changeSemanticItem() {
    this.showSemantic = !this.showSemantic;
    this.setSemanticItem();
  }

  showLoadingMenu() {
    this.menuOptionsService.setLoadingMenu(true);
    setTimeout(() => {
      this.menuOptionsService.setLoadingMenu(false);
    }, 2000);
  }
  private setSemanticItem() {
    const favoritos = this.menu.find(item => item.displayName === 'Favoritos');
    favoritos.isSemantic = this.showSemantic;
    this.menuOptionsService.setMenu(this.menu);
  }

}
