import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { BreadcumbItem } from 'src/app/shared/interfaces/breadcumb-item';

@Component({
  selector: 'app-breadcumb',
  templateUrl: './breadcumb.component.html',
  styleUrls: ['./breadcumb.component.scss']
})
export class BreadcumbComponent implements OnInit {

  @Input() breadcumbPath: BreadcumbItem[] = [];
  @Output() breadcumbSelected = new EventEmitter<BreadcumbItem>();

  constructor() { }

  ngOnInit(): void {
  }

  selectedItem(item: BreadcumbItem) {
    if (item.clicable) {
      this.breadcumbSelected.emit(item);
    }
  }

}
