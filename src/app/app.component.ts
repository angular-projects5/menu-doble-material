import { MediaMatcher } from '@angular/cdk/layout';
import { Component, ViewEncapsulation, ViewChild } from '@angular/core';
import { GlobalService } from './shared/services/global.service';
import { MenuItem, MenuItemActivable } from './shared/interfaces/menu-item';
import { MatSidenav } from '@angular/material/sidenav';
import { BreadcumbItem } from './shared/interfaces/breadcumb-item';
import { MenuOptionsService } from './shared/services/menu-options.service';
import { Logos } from './shared/interfaces/toolbar';
import { Router } from '@angular/router';
import { VariableClasses } from './shared/interfaces/style-class';
import { ChildrenTag } from './shared/interfaces/children-tag';
import { trigger, state, style, animate, transition } from '@angular/animations';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  animations: [
    trigger('hideText', [
      state('0', style({ display: 'block', opacity: 1 })),
      state('1', style({ opacity: 0, display: 'none' })),
      transition('0 => 1', animate('0.5s')),
      transition('1 => 0', animate('0.5s'))
    ]),
    trigger('fadeInOutFirstSlider', [
      state('0', style({ width: '250px' })),
      state('1', style({ width: '50px' })),
      transition('0 => 1', animate('.5s')),
      transition('1 => 0', animate('.5s'))
    ]),
    trigger('fadeInOutFirstContent', [
      state('0', style({ marginLeft: '250px' })),
      state('1', style({ marginLeft: '50px' })),
      transition('0 => 1', animate('.5s')),
      transition('1 => 0', animate('.5s'))
    ]),
    trigger('fadeInOutSecondSlider', [
      state('0', style({ width: '200px' })),
      state('1', style({ width: '30px' })),
      transition('0 => 1', animate('.3s')),
      transition('1 => 0', animate('.3s'))
    ]),
    trigger('fadeInOutSecondContent', [
      state('0', style({ marginLeft: '200px' })),
      state('1', style({ marginLeft: '30px' })),
      state('2', style({ marginLeft: '0px' })),
      transition('0 => 1', animate('.3s')),
      transition('1 => 0', animate('.3s'))
    ])
  ],
  encapsulation: ViewEncapsulation.None
})
export class AppComponent {
  @ViewChild('snav') sideNav: MatSidenav;
  @ViewChild('secondSnav') secondSideNav: MatSidenav;

  logoToolbar: string;
  logo: string;
  version = '1.0.0';
  classes: VariableClasses;
  loadingMenu = false;
  menu: MenuItemActivable[];

  childrenMark: ChildrenTag = ChildrenTag.circle;


  mobileQuery: MediaQueryList;

  breadcumbPath: BreadcumbItem[] = [];

  firstSideNavOpen = true;
  secondSideNavOpen = true;

  secondMenu: MenuItemActivable[];
  thirdMenu: MenuItemActivable[];

  // Variables de configuración
  menuLong = false;
  showSemantic = false;

  constructor(
    media: MediaMatcher,
    private globalService: GlobalService,
    private menuOptionsService: MenuOptionsService,
    private router: Router) {
    this.mobileQuery = media.matchMedia('(max-width: 600px)');
    this.setOptionsObservables();
  }

  private setOptionsObservables() {
    this.menuOptionsService.getLoadingMenu()
      .subscribe((isLoading: boolean) => this.loadingMenu = isLoading);
    this.menuOptionsService.getLogo()
      .subscribe((logos: Logos) => {
        this.logo = logos.logo;
        this.logoToolbar = logos.toolbarLogo;
      });

    this.menuOptionsService.getMenu()
      .subscribe((menu: MenuItem[]) => {
        this.menu = menu;
        if (menu.length) {
          this.navigate(this.menu[0]);

        }
      });

    this.menuOptionsService.setMenu(this.globalService.getNavMenu());


  }

  navigate(item: MenuItemActivable, isBreadcumb = false) {
    if (item.isSemantic || item.isSeparator) { return; }
    if (!item.active) {
      this.secondMenu = this.removeMenuItems(this.secondMenu);
      if (this.secondSideNav) {
        this.secondSideNav.close();
      }
    }

    if (!(item.children && item.children.length)) {
      this.secondMenu = this.removeMenuItems(this.secondMenu);
    } else if (this.secondMenu === item.children) {
      this.secondSideNavOpen = !this.secondSideNavOpen;

      this.breadcumbPath = [this.createBreadcumbItem(item, 0)];
      this.redirectRoute(item);
      this.thirdMenu = this.removeMenuItems(this.thirdMenu);
      if (isBreadcumb) {
        this.desactiveMenuItems(this.secondMenu);
      }
      return;
    } else {
      this.secondMenu = item.children;
      this.secondSideNavOpen = true;
      this.secondSideNav.open();
    }

    this.desactiveMenuItems(this.menu);
    item.active = true;
    this.thirdMenu = this.removeMenuItems(this.thirdMenu);
    this.breadcumbPath = [this.createBreadcumbItem(item, 0)];

    this.redirectRoute(item);
  }

  private removeMenuItems(menu: MenuItemActivable[]) {
    if (menu) {
      this.desactiveMenuItems(menu);
      return [];
    }
    return menu;
  }

  private desactiveMenuItems(menu: MenuItemActivable[]) {
    if (menu) {
      menu.map(item => item.active = false);
    }
  }

  navigateChildrenItem(item: MenuItemActivable, isBreadcumb = false) {
    if (item.isSemantic || item.isSeparator) { return; }
    this.desactiveMenuItems(this.secondMenu);
    item.active = true;

    this.thirdMenu = this.removeMenuItems(this.thirdMenu);

    this.breadcumbPath = [
      this.createBreadcumbItem(this.menu.find(itm => itm.active), 0),
      this.createBreadcumbItem(item, 1)];

    let routeItem = item;
    if (item.children && item.children.length && !item.route) {
      routeItem = item.children[0];
      this.breadcumbPath.find(breadcumb => breadcumb.level === 1).clicable = false;
      this.breadcumbPath.push(this.createBreadcumbItem(routeItem, 2));
      routeItem.active = true;
    } else {
      this.desactiveMenuItems(this.thirdMenu);
    }

    this.redirectRoute(routeItem);
    this.thirdMenu = item.children;
  }
  private redirectRoute(item: MenuItem) {
    if (item.route) {
      this.router.navigate([item.route]);
    }
  }

  toggleSideNav() {
    this.firstSideNavOpen = !this.firstSideNavOpen;
  }

  toggleSecondSideNav() {
    this.secondSideNavOpen = !this.secondSideNavOpen;
  }


  private createBreadcumbItem(item: MenuItem, level: number, clicable = true): BreadcumbItem {
    return {
      level,
      item,
      clicable
    };
  }

  breadcumbPathSelected(item: BreadcumbItem) {
    if (item.level === 0) {
      this.navigate(item.item, true);
      return;
    }
    if (item.level === 1) {
      this.navigateChildrenItem(item.item, true);
      return;
    }
    this.navBarChange(item.item);
  }

  navBarChange(item: MenuItemActivable) {
    this.desactiveMenuItems(this.thirdMenu);
    item.active = true;
    this.breadcumbPath = this.breadcumbPath.filter(breadcumb => breadcumb.level !== 2);
    this.breadcumbPath.push(this.createBreadcumbItem(item, 2));
  }

  public resetMenu() {
    this.breadcumbPath = [];
    this.secondMenu = this.removeMenuItems(this.secondMenu);
    this.thirdMenu = this.removeMenuItems(this.thirdMenu);
    this.secondSideNavOpen = false;
    this.secondSideNav.opened = false;
    this.desactiveMenuItems(this.menu);
  }



}
