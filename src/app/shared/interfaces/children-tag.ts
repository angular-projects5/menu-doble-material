export enum ChildrenTag {
    none = 'none',
    dots = 'dots',
    circle = 'circle'
}