export interface VariableClasses {
    textAppClass?: string;
    toolbarClass?: string;
    itemFirstLevelClass?: string;
    itemSecondLevelClass?: string;
    itemThirdLevelClass?: string;
    separatorChildrenClass?: string;
}
