export interface MenuItem {
    displayName: string;
    icon?: IconClass;
    route?: string;
    children?: MenuItem[];
    disabled?: boolean;
    isSemantic?: boolean;
    isSeparator?: boolean;
}

export interface IconClass {
    name: string;
    color: string;
    type?: IconStyle;
}

export enum IconStyle {
    normal = 'material-icons',
    outline = 'material-icons-outlined',
    twoTone = 'material-icons-two-tone',
    round = 'material-icons-round',
    sharp = 'material-icons-sharp',
}

export interface MenuItemActivable extends MenuItem {
    active?: boolean;
}
