import { MenuItem } from './menu-item';

export interface BreadcumbItem {
    level: number;
    item: MenuItem;
    clicable: boolean;
}