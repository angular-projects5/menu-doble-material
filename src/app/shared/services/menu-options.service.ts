import { Injectable } from '@angular/core';
import { Subject, Observable, BehaviorSubject } from 'rxjs';
import { Logos } from '../interfaces/toolbar';
import { MenuItem } from '../interfaces/menu-item';

@Injectable({
  providedIn: 'root'
})
export class MenuOptionsService {

  public logo: BehaviorSubject<Logos> = new BehaviorSubject<Logos>(new Logos());
  public menu: BehaviorSubject<MenuItem[]> = new BehaviorSubject<MenuItem[]>([]);
  private loadingMenu: Subject<boolean> = new Subject<boolean>();

  constructor() { }

  setLogo(logo: Logos) {
    this.logo.next(logo);
  }

  getLogo(): Observable<Logos> {
    return this.logo.asObservable();
  }
  setMenu(menu: MenuItem[]) {
    this.menu.next(menu);
  }

  getMenu(): Observable<MenuItem[]> {
    return this.menu.asObservable();
  }
  setLoadingMenu(isLoading: boolean) {
    this.loadingMenu.next(isLoading);
  }

  getLoadingMenu(): Observable<boolean> {
    return this.loadingMenu.asObservable();
  }
}
