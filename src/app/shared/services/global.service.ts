import { Injectable } from '@angular/core';
import { MenuItem, IconStyle } from '../interfaces/menu-item';

@Injectable({
  providedIn: 'root'
})
export class GlobalService {

  constructor() { }

  getNavMenu() {
    // const menu: MenuItem[] = [
    //   { displayName: 'Inicio', icon: { name: 'home', color: '#6393e8' }, route: 'home' },
    //   {
    //     displayName: 'Administrador de GIT', icon: { name: 'security', color: '#3adca8' }, children: [
    //       {
    //         displayName: 'Calendarios', children: [
    //           { displayName: 'Perfiles', route: 'profiles' },
    //           { displayName: 'Festivos', route: 'calendars' }
    //         ]
    //       },
    //       { displayName: 'Centros', route: 'centers' },
    //       {
    //         displayName: 'Tablas generales', children: [
    //           { displayName: 'Áreas', route: 'areas' },
    //           { displayName: 'Salario base', route: 'basic_salary' },
    //           { displayName: 'Tipos de incidencia', route: 'incidences_types' },
    //           { displayName: 'Tipos de incidencia visibles', route: 'incidences_visible' },
    //           { displayName: 'Períodos de pago', route: 'payment_period' },
    //           { displayName: 'Motivos de absentismo', route: 'reason_absence' },
    //           { displayName: 'Tipos de vacaciones', route: 'types_of_holiday' },
    //           { displayName: 'Puestos de trabajo', route: 'worksite' }
    //         ]
    //       },
    //       { displayName: 'Preferencias de centros', route: 'centers_preferences' },
    //       {
    //         displayName: 'Seguridad', children: [
    //           { displayName: 'Usuarios', route: 'users' }
    //         ]
    //       }
    //     ]
    //   },
    //   {
    //     displayName: 'Empleados', icon: { name: 'people', color: '#eccd93' }, children: [
    //       { displayName: 'Datos de empleado', route: 'employees' },
    //       { displayName: 'Fases de empleado', route: 'contracts' },
    //       { displayName: 'Documentos expirados', route: 'documents_to_expire' }
    //     ]
    //   },
    //   {
    //     displayName: 'Gestionar GIT', icon: { name: 'work', color: '#bbbbbb' }, children: [
    //       {
    //         displayName: 'Incidencias', children: [
    //           { displayName: 'Incidencias individuales', route: 'incidences_individual' },
    //           { displayName: 'Incidencias mensuales', route: 'incidences_monthly' }
    //         ]
    //       },
    //       { displayName: 'Vacaciones', route: 'holidays' },
    //       { displayName: 'Procesos' },
    //       { displayName: 'Gestión de pagos' },
    //       { displayName: 'Informes' },
    //       { displayName: 'Lista de incidencias de capataz' },
    //       { displayName: 'Lista de vacaciones de capataz' }
    //     ]
    //   }
    // ];
    // return menu;
    const menu: MenuItem[] = [
      { displayName: 'Inicio', icon: { name: 'home', color: '#6393e8', type: IconStyle.outline }, route: 'home' },
      { displayName: 'Registro de actividad', isSeparator: true },
      {
        displayName: 'Panel', icon: { name: 'security', color: '#3adca8' }, route: 'panel', children: [
          {
            displayName: 'Información general', icon: { name: 'info', color: 'blue' }, route: 'info-general', children: []
          },
          { displayName: 'Registro de actividad', isSeparator: true },
          { displayName: 'Entradas', icon: { name: 'sports_handball', color: 'blue' }, route: 'entradas' },
          { displayName: 'Parones', icon: { name: 'sports_kabaddi', color: 'grey' }, route: 'parones' },
          { displayName: 'Salidas', icon: { name: 'directions_walk', color: 'red' }, route: 'salidas' },
          {
            displayName: 'Eventos', icon: { name: 'highlight', color: '#c3c300' }, children: [
              { displayName: 'Camping', icon: { name: 'store_mall_directory', color: 'green' }, route: 'camping' },
              { displayName: 'Escalada', icon: { name: 'map', color: 'brown' }, route: 'escalada' }
            ]
          }
        ]
      },
      {
        displayName: 'Favoritos', icon: { name: 'star', color: '#eccd93' }, isSemantic: false
      },
      {
        displayName: 'Todos los servicios', icon: { name: 'people', color: '#bbbbbb' }, route: '', children: [
          { displayName: 'MENU.employee_data', route: 'employees' },
          { displayName: 'MENU.contracts', route: 'contracts' },
          { displayName: 'MENU.documents_to_expire', route: 'documents_to_expire' },
        ]
      },
      {
        displayName: 'Cuentas de almacenamiento', icon: { name: 'work', color: 'black' }, route: '', children: [
          {
            displayName: 'MENU.incidences', route: '', children: [
              { displayName: 'MENU.incidences_individual', route: 'incidences_individual' },
              { displayName: 'MENU.incidences_monthly', route: 'incidences_monthly' },
            ]
          },
          { displayName: 'MENU.holidays', route: 'holidays' },
          { displayName: 'MENU.processes', route: '' },
          { displayName: 'MENU.payment_management', route: '' },
          { displayName: 'MENU.reports', route: '' },
          { displayName: 'MENU.foreman_incidences', route: '' },
          { displayName: 'MENU.foreman_vacations', route: '' },
        ]
      }
    ];
    return menu;
  }

  getNavMenuLong() {
    const menu: MenuItem[] = [
      { displayName: 'Inicio', icon: { name: 'home', color: '#6393e8' }, route: 'home' },
      {
        displayName: 'Panel', icon: { name: 'security', color: '#3adca8' }, route: 'panel', children: [
          {
            displayName: 'Información general', icon: { name: 'info', color: 'blue' }, route: 'info-general', children: []
          },
          {
            displayName: 'Registro de actividadalioeiajsopije', icon: { name: 'highlight', color: '#3f51b5' }, children: [
              { displayName: 'Entradas', icon: { name: 'sports_handball', color: 'blue' }, route: 'entradas' },
              { displayName: 'Parones', icon: { name: 'sports_kabaddi', color: 'grey' }, route: 'parones' },
              { displayName: 'Salidas', icon: { name: 'directions_walk', color: 'red' }, route: 'salidas' },
              { displayName: 'Entradas', icon: { name: 'sports_handball', color: 'blue' }, route: 'entradas' },
              { displayName: 'Parones', icon: { name: 'sports_kabaddi', color: 'grey' }, route: 'parones' },
              { displayName: 'Salidas', icon: { name: 'directions_walk', color: 'red' }, route: 'salidas' },
              { displayName: 'Entradas', icon: { name: 'sports_handball', color: 'blue' }, route: 'entradas' },
              { displayName: 'Parones', icon: { name: 'sports_kabaddi', color: 'grey' }, route: 'parones' },
              { displayName: 'Salidas', icon: { name: 'directions_walk', color: 'red' }, route: 'salidas' },
              { displayName: 'Entradas', icon: { name: 'sports_handball', color: 'blue' }, route: 'entradas' },
              { displayName: 'Parones', icon: { name: 'sports_kabaddi', color: 'grey' }, route: 'parones' },
              { displayName: 'Salidas', icon: { name: 'directions_walk', color: 'red' }, route: 'salidas' },
              { displayName: 'Entradas', icon: { name: 'sports_handball', color: 'blue' }, route: 'entradas' },
              { displayName: 'Parones', icon: { name: 'sports_kabaddi', color: 'grey' }, route: 'parones' },
              { displayName: 'Salidas', icon: { name: 'directions_walk', color: 'red' }, route: 'salidas' },
              { displayName: 'Entradas', icon: { name: 'sports_handball', color: 'blue' }, route: 'entradas' },
              { displayName: 'Parones', icon: { name: 'sports_kabaddi', color: 'grey' }, route: 'parones' },
              { displayName: 'Salidas', icon: { name: 'directions_walk', color: 'red' }, route: 'salidas' },
            ]
          },
          {
            displayName: 'Eventos', icon: { name: 'highlight', color: '#c3c300' }, isSemantic: true
          },
          {
            displayName: 'Eventos', icon: { name: 'highlight', color: '#c3c300' }, route: 'eventos', children: [
              { displayName: 'Camping', icon: { name: 'store_mall_directory', color: 'green' }, route: 'camping' },
              { displayName: 'Escalada', icon: { name: 'map', color: 'brown' }, route: 'escalada' }
            ]
          },
          {
            displayName: 'Eventos', icon: { name: 'highlight', color: '#c3c300' }, route: 'eventos', children: [
              { displayName: 'Camping', icon: { name: 'store_mall_directory', color: 'green' }, route: 'camping' },
              { displayName: 'Escalada', icon: { name: 'map', color: 'brown' }, route: 'escalada' }
            ]
          },
          {
            displayName: 'Eventos', icon: { name: 'highlight', color: '#c3c300' }, route: 'eventos', children: [
              { displayName: 'Camping', icon: { name: 'store_mall_directory', color: 'green' }, route: 'camping' },
              { displayName: 'Escalada', icon: { name: 'map', color: 'brown' }, route: 'escalada' }
            ]
          },
          {
            displayName: 'Eventos', icon: { name: 'highlight', color: '#c3c300' }, route: 'eventos', children: [
              { displayName: 'Camping', icon: { name: 'store_mall_directory', color: 'green' }, route: 'camping' },
              { displayName: 'Escalada', icon: { name: 'map', color: 'brown' }, route: 'escalada' }
            ]
          },
          {
            displayName: 'Eventos', icon: { name: 'highlight', color: '#c3c300' }, route: 'eventos', children: [
              { displayName: 'Camping', icon: { name: 'store_mall_directory', color: 'green' }, route: 'camping' },
              { displayName: 'Escalada', icon: { name: 'map', color: 'brown' }, route: 'escalada' }
            ]
          },
          {
            displayName: 'Eventos', icon: { name: 'highlight', color: '#c3c300' }, route: 'eventos', children: [
              { displayName: 'Camping', icon: { name: 'store_mall_directory', color: 'green' }, route: 'camping' },
              { displayName: 'Escalada', icon: { name: 'map', color: 'brown' }, route: 'escalada' }
            ]
          },
          {
            displayName: 'Eventos', icon: { name: 'highlight', color: '#c3c300' }, route: 'eventos', children: [
              { displayName: 'Camping', icon: { name: 'store_mall_directory', color: 'green' }, route: 'camping' },
              { displayName: 'Escalada', icon: { name: 'map', color: 'brown' }, route: 'escalada' }
            ]
          },
          {
            displayName: 'Eventos', icon: { name: 'highlight', color: '#c3c300' }, route: 'eventos', children: [
              { displayName: 'Camping', icon: { name: 'store_mall_directory', color: 'green' }, route: 'camping' },
              { displayName: 'Escalada', icon: { name: 'map', color: 'brown' }, route: 'escalada' }
            ]
          },
          {
            displayName: 'Eventos', icon: { name: 'highlight', color: '#c3c300' }, route: 'eventos', children: [
              { displayName: 'Camping', icon: { name: 'store_mall_directory', color: 'green' }, route: 'camping' },
              { displayName: 'Escalada', icon: { name: 'map', color: 'brown' }, route: 'escalada' }
            ]
          },
          {
            displayName: 'Eventos', icon: { name: 'highlight', color: '#c3c300' }, route: 'eventos', children: [
              { displayName: 'Camping', icon: { name: 'store_mall_directory', color: 'green' }, route: 'camping' },
              { displayName: 'Escalada', icon: { name: 'map', color: 'brown' }, route: 'escalada' }
            ]
          },
          {
            displayName: 'Eventos', icon: { name: 'highlight', color: '#c3c300' }, route: 'eventos', children: [
              { displayName: 'Camping', icon: { name: 'store_mall_directory', color: 'green' }, route: 'camping' },
              { displayName: 'Escalada', icon: { name: 'map', color: 'brown' }, route: 'escalada' }
            ]
          },
          {
            displayName: 'Eventos', icon: { name: 'highlight', color: '#c3c300' }, route: 'eventos', children: [
              { displayName: 'Camping', icon: { name: 'store_mall_directory', color: 'green' }, route: 'camping' },
              { displayName: 'Escalada', icon: { name: 'map', color: 'brown' }, route: 'escalada' }
            ]
          },
          {
            displayName: 'Eventos', icon: { name: 'highlight', color: '#c3c300' }, route: 'eventos', children: [
              { displayName: 'Camping', icon: { name: 'store_mall_directory', color: 'green' }, route: 'camping' },
              { displayName: 'Escalada', icon: { name: 'map', color: 'brown' }, route: 'escalada' }
            ]
          },
          {
            displayName: 'Eventos', icon: { name: 'highlight', color: '#c3c300' }, route: 'eventos', children: [
              { displayName: 'Camping', icon: { name: 'store_mall_directory', color: 'green' }, route: 'camping' },
              { displayName: 'Escalada', icon: { name: 'map', color: 'brown' }, route: 'escalada' }
            ]
          },
          {
            displayName: 'Eventos', icon: { name: 'highlight', color: '#c3c300' }, route: 'eventos', children: [
              { displayName: 'Camping', icon: { name: 'store_mall_directory', color: 'green' }, route: 'camping' },
              { displayName: 'Escalada', icon: { name: 'map', color: 'brown' }, route: 'escalada' }
            ]
          },
          {
            displayName: 'Eventos', icon: { name: 'highlight', color: '#c3c300' }, route: 'eventos', children: [
              { displayName: 'Camping', icon: { name: 'store_mall_directory', color: 'green' }, route: 'camping' },
              { displayName: 'Escalada', icon: { name: 'map', color: 'brown' }, route: 'escalada' }
            ]
          },
          {
            displayName: 'Eventos', icon: { name: 'highlight', color: '#c3c300' }, route: 'eventos', children: [
              { displayName: 'Camping', icon: { name: 'store_mall_directory', color: 'green' }, route: 'camping' },
              { displayName: 'Escalada', icon: { name: 'map', color: 'brown' }, route: 'escalada' }
            ]
          },
          {
            displayName: 'Eventos', icon: { name: 'highlight', color: '#c3c300' }, route: 'eventos', children: [
              { displayName: 'Camping', icon: { name: 'store_mall_directory', color: 'green' }, route: 'camping' },
              { displayName: 'Escalada', icon: { name: 'map', color: 'brown' }, route: 'escalada' }
            ]
          },
          {
            displayName: 'Eventos', icon: { name: 'highlight', color: '#c3c300' }, route: 'eventos', children: [
              { displayName: 'Camping', icon: { name: 'store_mall_directory', color: 'green' }, route: 'camping' },
              { displayName: 'Escalada', icon: { name: 'map', color: 'brown' }, route: 'escalada' }
            ]
          },
          {
            displayName: 'Eventos', icon: { name: 'highlight', color: '#c3c300' }, route: 'eventos', children: [
              { displayName: 'Camping', icon: { name: 'store_mall_directory', color: 'green' }, route: 'camping' },
              { displayName: 'Escalada', icon: { name: 'map', color: 'brown' }, route: 'escalada' }
            ]
          }
        ]
      },
      {
        displayName: 'Favoritos', icon: { name: 'star', color: '#eccd93' }, isSemantic: false
      },
      {
        displayName: 'Todos los servicios', icon: { name: 'people', color: '#bbbbbb' }, route: '', children: [
          { displayName: 'MENU.employee_data', route: 'employees' },
          { displayName: 'MENU.contracts', route: 'contracts' },
          { displayName: 'MENU.documents_to_expire', route: 'documents_to_expire' },
        ]
      },
      {
        displayName: 'Cuentas de almacenamiento', icon: { name: 'work', color: 'black' }, route: '', children: [
          {
            displayName: 'MENU.incidences', route: '', children: [
              { displayName: 'MENU.incidences_individual', route: 'incidences_individual' },
              { displayName: 'MENU.incidences_monthly', route: 'incidences_monthly' },
            ]
          },
          { displayName: 'MENU.holidays', route: 'holidays' },
          { displayName: 'MENU.processes', route: '' },
          { displayName: 'MENU.payment_management', route: '' },
          { displayName: 'MENU.reports', route: '' },
          { displayName: 'MENU.foreman_incidences', route: '' },
          { displayName: 'MENU.foreman_vacations', route: '' },
        ]
      },
      {
        displayName: 'Favoritos', icon: { name: 'star', color: '#eccd93' }, isSemantic: false
      },
      {
        displayName: 'Favoritos', icon: { name: 'star', color: '#eccd93' }, isSemantic: false
      },
      {
        displayName: 'Favoritos', icon: { name: 'star', color: '#eccd93' }, isSemantic: false
      },
      {
        displayName: 'Favoritos', icon: { name: 'star', color: '#eccd93' }, isSemantic: false
      },
      {
        displayName: 'Favoritos', icon: { name: 'star', color: '#eccd93' }, isSemantic: false
      },
      {
        displayName: 'Favoritos', icon: { name: 'star', color: '#eccd93' }, isSemantic: false
      },
      {
        displayName: 'Favoritos', icon: { name: 'star', color: '#eccd93' }, isSemantic: false
      },
      {
        displayName: 'Favoritos', icon: { name: 'star', color: '#eccd93' }, isSemantic: false
      },
      {
        displayName: 'Favoritos', icon: { name: 'star', color: '#eccd93' }, isSemantic: false
      },
      {
        displayName: 'Favoritos', icon: { name: 'star', color: '#eccd93' }, isSemantic: false
      },
      {
        displayName: 'Favoritos', icon: { name: 'star', color: '#eccd93' }, isSemantic: false
      },
      {
        displayName: 'Favoritos', icon: { name: 'star', color: '#eccd93' }, isSemantic: false
      },
      {
        displayName: 'Favoritos', icon: { name: 'star', color: '#eccd93' }, isSemantic: false
      },
      {
        displayName: 'Favoritos', icon: { name: 'star', color: '#eccd93' }, isSemantic: false
      },
      {
        displayName: 'Favoritos', icon: { name: 'star', color: '#eccd93' }, isSemantic: false
      },
      {
        displayName: 'Favoritos', icon: { name: 'star', color: '#eccd93' }, isSemantic: false
      },
      {
        displayName: 'Favoritos', icon: { name: 'star', color: '#eccd93' }, isSemantic: false
      },
      {
        displayName: 'Favoritos', icon: { name: 'star', color: '#eccd93' }, isSemantic: false
      },
      {
        displayName: 'Favoritos', icon: { name: 'star', color: '#eccd93' }, isSemantic: false
      },
      {
        displayName: 'Favoritos', icon: { name: 'star', color: '#eccd93' }, isSemantic: false
      },
      {
        displayName: 'Favoritos', icon: { name: 'star', color: '#eccd93' }, isSemantic: false
      },
      {
        displayName: 'Favoritos', icon: { name: 'star', color: '#eccd93' }, isSemantic: false
      },
      {
        displayName: 'Favoritos', icon: { name: 'star', color: '#eccd93' }, isSemantic: false
      },
      {
        displayName: 'Favoritos', icon: { name: 'star', color: '#eccd93' }, isSemantic: false
      },
      {
        displayName: 'Favoritos', icon: { name: 'star', color: '#eccd93' }, isSemantic: false
      }
    ];
    return menu;
  }
}
