import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ExampleComponent } from './components/example/example.component';
import { PanelComponent } from './components/panel/panel.component';
import { InicioComponent } from './components/inicio/inicio.component';
import { InfoGeneralComponent } from './components/info-general/info-general.component';
import { CampingComponent } from './components/camping/camping.component';
import { EntradasComponent } from './components/entradas/entradas.component';
import { EscaladaComponent } from './components/escalada/escalada.component';
import { EventosComponent } from './components/eventos/eventos.component';
import { ParonesComponent } from './components/parones/parones.component';
import { SalidasComponent } from './components/salidas/salidas.component';


const routes: Routes = [
  { path: '', component: ExampleComponent },
  { path: 'home', component: InicioComponent },
  { path: 'panel', component: PanelComponent },
  { path: 'info-general', component: InfoGeneralComponent },
  { path: 'camping', component: CampingComponent },
  { path: 'entradas', component: EntradasComponent },
  { path: 'escalada', component: EscaladaComponent },
  { path: 'eventos', component: EventosComponent },
  { path: 'parones', component: ParonesComponent },
  { path: 'salidas', component: SalidasComponent },
  { path: '**', redirectTo: 'home', pathMatch: 'full' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
